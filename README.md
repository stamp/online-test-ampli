STAMP Online Amplifier
======================
STAMP Online Amplifier is a set of microservices to derive unit test from production stacktraces for crash reproduction. In this way you will have an effective tool to amplify your test cases with information coming from production log files. This amplifier exploits Evocrash tool.

## License
STAMP services are freely available under the [LGPL license](LICENSE).